<?php


use Illuminate\Support\Facades\Route;
function set_active_header($uri, $output = 'menu-item-here')
{
 if( is_array($uri) ) {
   foreach ($uri as $u) {
     if (Route::is($u)) {
       return $output;
     }
   }
 } else {
   if (Route::is($uri)){
     return $output;
   }
 }
}