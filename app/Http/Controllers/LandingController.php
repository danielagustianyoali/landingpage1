<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingController extends Controller
{
    //
    public function homepage(){
        return view('pages.homepage');
    }
    public function aboutPage(){
        return view('pages.about');
    }
    public function reachUs(){
        return view('pages.reach-us');
    }
    public function productPage(){
        return view('pages.product');
    }
    public function promoPage(){
        return view('pages.promo');
    }
}

