@extends('master')

@section('cssPage')
@endsection

@section('content')
    <div class="d-flex flex-column-fluid content">
        <div class="container">
            <div class="card mt-7 mb-7">
                <div class="card-body">
                    <h1 class="title-part">Promo dan Event</h1>
                    <div class="row">
                        @for ($i = 0; $i < 8; $i++)
                            
                            @component('components.custom.promo-box')
                                @slot('title')
                                    LoremIpsum {{$i}}
                                @endslot
                                @slot('detail')
                                    LoremIpsum LoremIpsum LoremIpsum LoremIpsum LoremIpsum LoremIpsum LoremIpsum 
                                @endslot
                            @endcomponent   

                        @endfor
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titleModal">Lorem Ipsum</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <p id="detailModal">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                <div class="modal-footer">
                    
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsPage')
    <script>
        $('.btn__primary').click(function(){
            $('#modalDetail').modal('show');
        })
    </script>
@endsection
