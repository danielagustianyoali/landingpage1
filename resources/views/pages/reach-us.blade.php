@extends('master')

@section('cssPage')
@endsection

@section('content')
    <div class="d-flex flex-column-fluid content">
        <div class="container">
            <div class="card card-reach mt-7 mb-7">
                <div class="card-body">
                    <h1 class="title-part">Hubungi Kami</h1>
                    <div class="row mt-7">
                        <div class="col-md-6 mx-auto">
                            <form action="">
                                <h5 class="title-part" style="text-align: start"></h5>
                                <input type="text" class="form-control" placeholder="Nama">
                                <input type="text" class="form-control" placeholder="Nomor Handphone">

                                <input type="text" class="form-control" placeholder="Email">

                                <select name="" id="" class="form-control">
                                    <option value="" selected>Customer</option>
                                    <option value="">Distributor</option>
                                </select>
                                <textarea name="" id="" cols="30" rows="10" class="form-control" placeholder="Isi Pesan"></textarea>
                                <button class="btn btn__primary">Kirim Pesan</button>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="card alamat mb-10 mt-5">
                                <div class="card-body">
                                    <h3>Ruko Crytsal 8 no 17, Alam Sutera, PakuAlam, Kec. Serpong Utara, Banten,  Indonesia</h3>
                                </div>
                            </div>
                            <div class="d-flex justify-content-center mb-4">
                                <h4 class="data">
                                    Ikuti Sosial Media Kami
                                </h4>
                            </div>
                            <div class="d-flex justify-content-center align-items-center">
                                <a href="" class="icon-data">
                                    <img src="https://seeklogo.com/images/W/whatsapp-icon-logo-BDC0A8063B-seeklogo.com.png" alt="">
                                </a>
                                <a href="" class="icon-data">
                                    <img src="https://seeklogo.com/images/F/facebook-logo-966BBFBC34-seeklogo.com.png" alt="">
                                </a>
                                <a href="" class="icon-data">
                                    <img src="https://seeklogo.com/images/Y/youtube-icon-logo-521820CDD7-seeklogo.com.png" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsPage')

@endsection
