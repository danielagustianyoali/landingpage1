@extends('master')

@section('cssPage')
@endsection

@section('content')
    <div class="d-flex flex-column-fluid content">
        <div class="container">
            <div class="card mt-7 mb-7">
                <div class="card-body">
                    <h1 class="title-part">Siapakah Kami?</h1>
                     <div class="row mt-7">
                         <div class="col-md-5 mx-auto">
                             <div class="d-flex justify-content-center align-items-center">
                                <img src="https://www.southcharlottefamilycounseling.com/wp-content/uploads/2015/10/cropped-logo-dummy-300x300.png" alt="" class="img-about">

                             </div>
                         </div>
                         <div class="col-md-7">
                             <h3 class="title-part red" style="text-align: start">Warisan Gajah Mada</h3>
                             <p style="text-align: justify"> <strong>Warisan Gajah Mada</strong>  adalah  Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing
                                elit, sed do eiusmod tempor
                                incididunt ut labore et dolore magna aliqua.</p>
                         </div>
                     </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('jsPage')

@endsection
