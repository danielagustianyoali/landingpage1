@extends('master')

@section('cssPage')
@endsection

@section('content')
    <div class="d-flex flex-column-fluid content">
        <div class="container">
            <div class="card  mt-7 mb-7">
                <div class="card-body">
                    <h1 class="title-part mb-6">Our Product</h1>
                    <div class="row">
                        <div class="col-md-3">
                            <div class="card mb-5">
                                <div class="card-body">
                                    <div class="mt-5">
                                        <h5 class="kategori-title"> <u>Kategori</u> </h5>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product">
                                        <label for="vehicle1" class="label-checkbox"> I have a bike</label><br>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product">
                                        <label for="vehicle1" class="label-checkbox"> I have a bike</label><br>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product" >
                                        <label for="vehicle1"  class="label-checkbox"> I have a bike</label><br>
                                    </div>
                                    <div class="mt-5">
                                        <h5 class="kategori-title"> <u>Harga</u> </h5>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product">
                                        <label for="vehicle1"> I have a bike</label><br>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product">
                                        <label for="vehicle1"> I have a bike</label><br>
                                        <input type="checkbox" id="vehicle1" name="vehicle1" value="Bike"
                                            class="checkbox-product">
                                        <label for="vehicle1"> I have a bike</label><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9">
                            <div class="scrollmenu">
                                @for ($i = 0; $i < 6; $i++)
                                    
                                @component('components.custom.product')
                                    @slot('title')
                                        Lorem IPsum Dolor
                                    @endslot
                                    @slot('harga')
                                        60.000
                                    @endslot
                                @endcomponent

                                @endfor
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalDetail" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="titleModal">Lorem Ipsum</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row"> 
                        <div class="col-md-5">
                            <img src="https://cf.shopee.co.id/file/ccf509eab1949567f49068a34d1f5481" alt="" class="img-modal">
                        </div>
                        <div class="col-md-7">
                            <p id="detailModal">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

                        </div>
                    </div>

                    <h6 class="mt-6 mt-4">Bisa Beli di:</h6>
                    <div class="toko-online ">
                        <a href="" class="btn tokped">
                            <img src="https://warisangajahmada.com/./images/tokped.svg" alt="">
                            Tokopedia
                        </a>
                        <a href="" class="btn shopee">
                            <img src="https://warisangajahmada.com/./images/sopi.svg" alt="">
                            Shopee
                        </a>
                        <a href="" class="btn blibli">
                            <img src="https://warisangajahmada.com/./images/blibli.svg" alt="">
                            Blibli
                        </a>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection

@section('jsPage')
    <script>
        $('.btn__primary').click(function(){
            $('#modalDetail').modal('show');
        })
    </script>
@endsection
