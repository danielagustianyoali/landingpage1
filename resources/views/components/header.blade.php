<!--begin::Header-->
<div id="kt_header" class="header header-fixed">

    <!--begin::Container-->
    <div class="container d-flex align-items-stretch justify-content-between">

        <!--begin::Left-->
        <div class="d-flex align-items-stretch mr-3">

            <!--begin::Header Logo-->
            <div class="header-logo">
                <a href="{{route('homepage')}}">
                    <img alt="Logo" src="https://warisangajahmada.com/images/logo.svg" class="logo-default max-h-40px" />
                    {{-- <img alt="Logo" src="assets/media/logos/logo-letter-1.png" class="logo-sticky max-h-40px" /> --}}
                </a>
            </div>

            <!--end::Header Logo-->

            <!--begin::Header Menu Wrapper-->
           

            <!--end::Header Menu Wrapper-->
        </div>

        <!--end::Left-->

        <!--begin::Topbar-->
		<div class="header-menu-wrapper header-menu-wrapper-right" id="kt_header_menu_wrapper">

			<!--begin::Header Menu-->
			<div id="kt_header_menu"
				class="header-menu header-menu-left header-menu-mobile header-menu-layout-default">

				<!--begin::Header Nav-->
				<ul class="menu-nav">
					<li class="menu-item {{set_active_header('aboutUs')}} menu-item-submenu menu-item-rel" onclick="moveWindow('about')" 
						>
						<a href="{{route('aboutUs')}}" class="menu-link menu-toggle">
							<span class="menu-text">About Us</span>
						</a>

					</li>
					<li class="menu-item {{set_active_header('productPage')}} menu-item-submenu menu-item-rel" onclick="moveWindow('product')" 
						>
						<a href="{{route('productPage')}}" class="menu-link menu-toggle">
							<span class="menu-text">Our Product</span>
							<span class="menu-desc"></span>
						</a>

					</li>
					<li class="menu-item {{set_active_header('promoPage')}} menu-item-submenu menu-item-rel" onclick="moveWindow('promo')" 
						>
						<a href="{{route('promoPage')}}" class="menu-link menu-toggle">
							<span class="menu-text">Promo Event</span>
							<span class="menu-desc"></span>
						
						</a>

					</li>
					<li class="menu-item {{set_active_header('reachUs')}} menu-item-submenu menu-item-rel" onclick="moveWindow('reach-us')" 
						>
						<a href="{{route('reachUs')}}" class="menu-link menu-toggle">
							<span class="menu-text">Reach Us</span>
							<span class="menu-desc"></span>
							<i class="menu-arrow"></i>
						</a>

					</li>
					
				</ul>

				<!--end::Header Nav-->
			</div>

			<!--end::Header Menu-->
		</div>

        <!--end::Topbar-->
    </div>

    <!--end::Container-->
</div>

<!--end::Header-->
