<div class="col-md-4 mt-5">
    <div class="card card-promo">
        <div class="card-body">
            <div class="d-flex justify-content-end flex-column">
                <h4 class="title-part  mt-5 mb-8" style="text-align: start">{{$title}}</h4>
                <div class="d-flex">
                    <button class="btn btn__primary " data-detail="{{$detail}}">Detail Promo</button>

                </div>
            </div>
        </div>
    </div>
</div>