<div class="card-product">
    <div class="card ">
        <div class="card-body">
            <img src="https://cf.shopee.co.id/file/ccf509eab1949567f49068a34d1f5481" alt="" class="img-product">
            <div class="px-2 py-2">
                <h5 class="title-product mb-3">{{ $title }}</h5>
                <h6 class="harga">Rp. {{ $harga }}</h6>
            </div>


        </div>
    </div>
    <div class="d-flex justify-content-center mt-5">
        <button class="btn btn__primary text-center">
            Detail Produk
        </button>
    </div>

</div>
