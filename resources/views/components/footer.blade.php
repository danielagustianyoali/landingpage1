<!--begin::Footer-->
<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">

    <!--begin::Container-->
    <div class="container d-flex flex-column flex-md-row align-items-center justify-content-between mt-6 mb-6">

        <!--begin::Copyright-->
        <div class="copyright-box ">
            <img src="https://warisangajahmada.com/images/footer-logo.png" alt="" class="img-fluid logo-footer">
            <div class="link-div">
                <a href="http://keenthemes.com/metronic" target="_blank" class="link ">About Us</a>
                <a href="http://keenthemes.com/metronic" target="_blank" class="link ">Our Product</a>
                <a href="http://keenthemes.com/metronic" target="_blank" class="link ">Promo Event</a>
                <a href="http://keenthemes.com/metronic" target="_blank" class="link ">Reach Us</a>

            </div>
        </div>

        <!--end::Copyright-->

        <div >
            <h5 class="title mb-5">Temukan Kami Di</h5>
            <div class="toko-online ">
                <a href="" class="btn tokped">
                    <img src="https://warisangajahmada.com/./images/tokped.svg" alt="">
                    Tokopedia
                </a>
                <a href="" class="btn shopee">
                    <img src="https://warisangajahmada.com/./images/sopi.svg" alt="">
                    Shopee
                </a>
                <a href="" class="btn blibli">
                    <img src="https://warisangajahmada.com/./images/blibli.svg" alt="">
                    Blibli
                </a>
            </div>
        </div>

        <!--end::Nav-->
    </div>

    <!--end::Container-->
</div>

<!--end::Footer-->
